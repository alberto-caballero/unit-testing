import { Observable, of } from 'rxjs';

export class MockDataService {
    getTodo(): Observable<any> {
        return of({});
    }
}
